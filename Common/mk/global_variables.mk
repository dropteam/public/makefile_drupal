#!/usr/bin/make -f

DRUPAL_BASE_URL=http://localhost
##### Paths
##### -----
GIT_DIR=../
PROJECT_ROOT :=../src
VENDOR_ROOT=${PROJECT_ROOT}/vendor

APP_ROOT=${PROJECT_ROOT}/web
APP_URL=http://127.0.0.1

WEBSITE_CONFIG_PATH = ${PROJECT_ROOT}
WEBSITE_CONFIG_DIRECTORY = config/sync

# Paths to executables.
DRUSH=${VENDOR_ROOT}/bin/drush -r $(APP_ROOT)
DRUPAL=${VENDOR_ROOT}/bin/drupal
COMPOSER=composer

# Files and directories inside the Drupal installation.
WEBSITE_SITES_DIR=${APP_ROOT}/sites
WEBSITE_SETTINGS_DIR=${WEBSITE_SITES_DIR}/default
WEBSITE_SETTINGS_PHP_DEFAULT=${WEBSITE_SETTINGS_DIR}/settings.default.php
WEBSITE_SETTINGS_PHP=${WEBSITE_SETTINGS_DIR}/settings.php
WEBSITE_SETTINGS_LOCAL_EXAMPLE = ${WEBSITE_SITES_DIR}/example.settings.local.php
WEBSITE_SETTINGS_LOCAL = ${WEBSITE_SETTINGS_DIR}/settings.local.php
WEBSITE_FILES_DIR = sites/default/files

WEBSITE_MODULES_DIR = ${APP_ROOT}/modules
WEBSITE_THEMES_DIR = ${APP_ROOT}/themes
WEBSITE_PROFILES_DIR = ${APP_ROOT}/profiles

CONFIG_DIR_OLD= config_directories = \[\]\;
CONFIG_DIR_OLD_INSTALLED = config_directories\['sync'\].*
CONFIG_DIR_NEW = config_directories['sync'] = '${PROJECT_ROOT}/${WEBSITE_CONFIG_DIRECTORY}';

##### DB dump rollback variables
##### ----------------------------
DB_DUMP_ROLLBACK_DIR = ${PROJECT_ROOT}/db-dump/dbdump-rollback
DB_DUMP_ROLLBACK_NAME = db-rollback.sql

# Module enable/disbale
DRUPAL_DEV_MODULE = field_ui menu_ui views_ui dblog
DRUPAL_NO_PROD_MODULE = field_ui menu_ui views_ui dblog

# Change this list to uninstall module before run composer.
# DRUPAL_PMU_FIRST_MODULE =

ROBOT_TXT_DENY = 1
DRUPAL_DEPLOY_ID=1.0


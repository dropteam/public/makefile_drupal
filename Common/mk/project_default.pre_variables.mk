#!/usr/bin/make -f
#!/#### Please copy this file as project_variables.mk when you are in a development environment.

#### Project specific
#### ----------------
PROJECT_NAME = druapl

#### Drupal configuration
#### --------------------
WEBSITE_PROFILE_NAME = drupal_profile

# The site name.
WEBSITE_SITE_NAME = "DRUPAL site"

# Database settings.
DRUPAL_DB_TYPE = mysql
# following setting is for travis, please use something like ${project.name}
DRUPAL_DB_NAME = ${PROJECT_NAME}
# following setting is for travis, please use something like ${project.name}_user
DRUPAL_DB_USER = ${PROJECT_NAME}_user
DRUPAL_DB_PASSWORD = mysqlpwd
DRUPAL_DB_HOST = database
DRUPAL_DB_PORT = 3306
DRUPAL_DB_URL = ${DRUPAL_DB_TYPE}://${DRUPAL_DB_USER}:${DRUPAL_DB_PASSWORD}@${DRUPAL_DB_HOST}:${DRUPAL_DB_PORT}/${DRUPAL_DB_NAME}

# Admin user.
DRUPAL_ADMIN_USERNAME = ${PROJECT_NAME}_admin
DRUPAL_ADMIN_PASSWORD = ${PROJECT_NAME}_2018
DRUPAL_ADMIN_EMAIL = juergen.pecher@mydropteam.com

# Please comment out if you need to set the site uuid in case you work in a team and you didn't import an initial database.
# See core-bug https://www.drupal.org/node/2583113 for the necessity to set in this case also the shortcut.uuid
WEBSITE_UUID = 4525bf4a-8851-4b0b-ac12-838d22da2cb9
SHORTCUT_UUID = f12e8b45-73cc-4dc4-b335-2f42d7eab26d
LANGUAGE_UUID = 31d342f9-a74f-47cf-9815-2d44920adee7
#Make sure to have double $$ to escape it
TRUSTED_HOST_PATTERNS = "['^drupal\.pre\.bo\.dev-dropteam\.com$$'];"

ROBOT_TXT_DENY = 1
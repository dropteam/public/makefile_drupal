#!/usr/bin/make -f
#!/#### Please copy this file as project_variables.mk when you are in a development environment.

#### Project specific
#### ----------------
PROJECT_NAME = ${COMPOSE_PROJECT_NAME}

#### Drupal configuration
#### --------------------
WEBSITE_PROFILE_NAME = drupal_profile

# The site name.
WEBSITE_SITE_NAME = "${COMPOSE_PROJECT_NAME}"

# Database settings.
DRUPAL_DB_TYPE = mysql
# following setting is for travis, please use something like ${project.name}
DRUPAL_DB_NAME = ${MYSQL_DATABASE}
# following setting is for travis, please use something like ${project.name}_user
DRUPAL_DB_USER = ${MYSQL_USER}
DRUPAL_DB_PASSWORD = ${MYSQL_PASSWORD}
DRUPAL_DB_HOST = database
DRUPAL_DB_PORT = 3306
DRUPAL_DB_URL = ${DRUPAL_DB_TYPE}://${DRUPAL_DB_USER}:${DRUPAL_DB_PASSWORD}@${DRUPAL_DB_HOST}:${DRUPAL_DB_PORT}/${DRUPAL_DB_NAME}

# Admin user.
DRUPAL_ADMIN_USERNAME = ${PROJECT_NAME}_admin
DRUPAL_ADMIN_PASSWORD = ${PROJECT_NAME}_2019
DRUPAL_ADMIN_EMAIL = DEFINE@ADMIN.EMAIL

# Please comment out if you need to set the site uuid in case you work in a team and you didn't import an initial database.
# See core-bug https://www.drupal.org/node/2583113 for the necessity to set in this case also the shortcut.uuid
WEBSITE_UUID = 4525bf4a-8851-4b0b-ac12-838d22da2cb9
SHORTCUT_UUID = f12e8b45-73cc-4dc4-b335-2f42d7eab26d

ROBOT_TXT_DENY = 1
DRUPAL_DEPLOY_ID=1.0
#Make sure to have double $$ to escape it
TRUSTED_HOST_PATTERNS = "['^web\.local$$'];"


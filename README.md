# Makefile for Drupal 8

## Introduction

@todo

## Prerequisites

@todo
You must to have installed drush and composer on your operating system before use this makefile.

## Configuration

@todo

Edit and replace all variables in relation to your project inside all files into "mk" folder.

##  Common features

### Install

* Install Drupal depending the environnement
* Install module, and libraries with composer
* Write initial configuration inside settings.php
* Create and set permission on file directory
* Create and set configuration directory
### Deploy

* Clear cache drupal
* Set and remove the maintenance mod
* Install module, and libraries with composer
* Save database before deploying
* Run the database update (drush updb)
* Run the configuration synchronisation (drush cim)
* Run the entities update (depreciated since 8.7) (drush entup)
* Run the local translation update
* Create the services.yml
* Disable/ enable developper module depending environnement
* Copy an all deny robot.txt in case of not the production environnement

## How to use

After configured you need to go where you extract the makefile then run command make <command_name>

## Available commands

### Main command install

*        make install-local
*        make install-dev
*        make install-prod


### Main command deploy

*        make deploy-local
*        make pre-deploy-dev
*        make post-deploy-dev
*        make pre-deploy-prod
*        make post-deploy-prod


### Sub command install

This command are packaged and run by the main command, but you can also run one specific command if you need it.
This command are install command and also should be run only once.

*        make install-drupal
*        make install-composer-dependencies
*        make writable-settings-php
*        make build-dev
*        make setup-dev
*        make enable-dev-settings
*        make create-files-dir
*        make set-settings-files-dir
*        make set-permissions-files-dir
*        make create-config-dir 
*        make set-config-dir
*        make set-site-uuid
*        make set-shortcut-uuid
*        make set-language-uuid
*        make set-trusted-host-patterns

### Sub command deploy

This command are packaged and run by the main command, but you can also run one specific command if you need it.

*        make copy-services
*        make clear-cache
*        make db-dump-save-rollback
*        make update-db
*        make import-config
*        make enable-dev-module
*        make no-prod-module
*        make maintenance-on
*        make maintenance-off
*        make set-permissions
*        make locale-update
*        make deny-robot-txt
*        make set-deploy-id
